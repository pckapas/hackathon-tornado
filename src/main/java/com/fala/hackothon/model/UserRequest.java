package com.fala.hackothon.model;

import lombok.Data;
@Data
public class UserRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String Password;
    private boolean jira;
    private boolean slack;
}