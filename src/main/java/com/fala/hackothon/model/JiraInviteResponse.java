package com.fala.hackothon.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class JiraInviteResponse {
    public String self;
    public String accountId;
    public String accountType;
    public String emailAddress;
    public Object avatarUrls;
    public String displayName;
    public boolean active;
    public String timeZone;
    public String locale;
    public Object groups;
    public Object applicationRoles;
    public String expand;
}