package com.fala.hackothon.service;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import com.fala.hackothon.entity.PasswordProfile;
import com.fala.hackothon.entity.User;
import com.fala.hackothon.entity.UserEntity;
import com.fala.hackothon.entity.UserResponse;
import com.fala.hackothon.model.UserRequest;
import com.fala.hackothon.repository.UserRepository;
import com.microsoft.aad.msal4j.ClientCredentialFactory;
import com.microsoft.aad.msal4j.ClientCredentialParameters;
import com.microsoft.aad.msal4j.ConfidentialClientApplication;
import com.microsoft.aad.msal4j.IAuthenticationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AzureAdService {

    @Value("${azuread.authority}")
    private String authority;

    @Value("${azuread.clientid}")
    private String clientId;

    @Value("${azuread.secret}")
    private String secret;

    @Value("${azuread.scope}")
    private String scope;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RestTemplate restTemplate;

    public void createUser(UserRequest request) throws Exception {
        User user = new User();
        user.setDisplayName(String.format("%s %s",request.getFirstName(),request.getLastName()));
        user.setAccountEnabled(true);
        user.setUserPrincipalName(request.getEmail());
        user.setMailNickname(String.format("%s%s",request.getFirstName(),request.getLastName()));
        PasswordProfile passwordProfile = new PasswordProfile();
        passwordProfile.setPassword(request.getPassword());
        passwordProfile.setForceChangePasswordNextSignIn(true);
        user.setPasswordProfile(passwordProfile);
        String requestURI = "https://graph.microsoft.com/v1.0/users";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", String.format("%s %s", "Bearer", getAccessTokenByClientCredentialGrant().accessToken()));
        headers.add("Content-Type", "application/json");
        headers.add("Accept","application/json");
        HttpEntity<User> httpEntity = new HttpEntity<>(user, headers);
        ResponseEntity<UserResponse> response = restTemplate.exchange(requestURI, HttpMethod.POST, httpEntity, UserResponse.class);
        createUser(response.getBody());
    }

    public void deleteUser(String id) throws Exception {
        String requestURI = String.format("https://graph.microsoft.com/v1.0/users/%s",id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", String.format("%s %s", "Bearer", getAccessTokenByClientCredentialGrant().accessToken()));
        HttpEntity<?> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(requestURI, HttpMethod.DELETE, httpEntity,String.class);
        userRepository.deleteByEmailId(id);
    }

    public UserResponse getUser(String id) throws Exception {
        String requestURI = String.format("https://graph.microsoft.com/v1.0/users/%s",id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", String.format("%s %s", "Bearer", getAccessTokenByClientCredentialGrant().accessToken()));
        headers.add("Content-Type", "application/json");
        HttpEntity<?> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<UserResponse> response = restTemplate.exchange(requestURI, HttpMethod.GET, httpEntity,UserResponse.class);
        System.out.println(response.getBody());
        return response.getBody();
    }

    public void createUser(UserResponse userResponse){
        UserEntity entity = new UserEntity();
        entity.setAzureAdId(userResponse.getId());
        entity.setUserName(userResponse.getDisplayName());
        entity.setEmailId(userResponse.getUserPrincipalName());
        userRepository.save(entity);
    }

    private IAuthenticationResult getAccessTokenByClientCredentialGrant() throws Exception {

        ConfidentialClientApplication app = ConfidentialClientApplication.builder(
                        clientId,
                        ClientCredentialFactory.createFromSecret(secret))
                .authority(authority)
                .build();

        // With client credentials flows the scope is ALWAYS of the shape "resource/.default", as the
        // application permissions need to be set statically (in the portal), and then granted by a tenant administrator
        ClientCredentialParameters clientCredentialParam = ClientCredentialParameters.builder(
                        Collections.singleton(scope))
                .build();

        CompletableFuture<IAuthenticationResult> future = app.acquireToken(clientCredentialParam);
        return future.get();
    }

}
