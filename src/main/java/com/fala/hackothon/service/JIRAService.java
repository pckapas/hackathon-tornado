package com.fala.hackothon.service;

import com.fala.hackothon.entity.UserEntity;
import com.fala.hackothon.model.JiraInviteResponse;
import com.fala.hackothon.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Base64;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import com.fala.hackothon.entity.UserInvite;
import com.fala.hackothon.exception.JiraInviteException;
import com.fala.hackothon.model.Response;

@Service
public class JIRAService {

	@Value("${create.user.uri}")
	private String createJiraUserURI;
	
	@Value("${jira.host.uri}")
	private String jiraHostURI;
	
	@Value("${jira.admin.username}")
	private String adminUserName;
	
	@Value("${jira.api.key}")
	private String apiKey;
	
	private static final String inviteSuccess = "Send Successfully Invited on Jira to";
	private static final String revokeSuccess = "Revoke access Successfully on Jira to";
	@Autowired
	private UserRepository userRepository;



	UserEntity userEntity = null;
	@SneakyThrows
	public Response sendInvitationFromJira(String userMailId) {
		 RestTemplate restTemplate=new RestTemplate();
		UserInvite userInvite= validateRequest(userMailId);
		String stringToBeEncode =String.format("%s%s%s", adminUserName,":", apiKey) ;
		String requestURI = String.format("%s%s", jiraHostURI,createJiraUserURI);
		String encodeString = new String(Base64.getEncoder().encode(stringToBeEncode.getBytes()));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", String.format("%s %s", "Basic", encodeString));
		HttpEntity<UserInvite> httpEntity = new HttpEntity<>(userInvite, headers);
		ResponseEntity<String> response=restTemplate.exchange(requestURI, HttpMethod.POST, httpEntity, String.class);
		
		if(response == null || response.getStatusCode().equals(HttpStatus.CREATED)) {

			ObjectMapper objectMapper=new ObjectMapper();
			JiraInviteResponse response1= objectMapper.readValue(response.getBody().toString(), JiraInviteResponse.class);
			userEntity.setJiraId(response1.getAccountId());
			userRepository.save(userEntity);
			return buildResponse(userInvite);
		}else {
			throw JiraInviteException.somethingwentwrong(); 
		}
	}


	private Response buildResponse(UserInvite userInvite) {
		Response response=new Response();
		response.setMessage(String.format("%s %s", inviteSuccess, userInvite.getDisplayName()));
		response.setStatusCode(HttpStatus.OK.value());
		return response;
	}


	private UserInvite validateRequest(String userMailId) {

		if(!StringUtils.hasText(userMailId)) {
			throw JiraInviteException.missingField("userMail");
		}

		userEntity=userRepository.findByEmailId(userMailId);

		if(userEntity == null){
			throw JiraInviteException.userNotFound();
		}
		if(!StringUtils.hasText(userEntity.getUserName())) {
			throw JiraInviteException.missingField("displayName");
		}
		UserInvite userInvite = new UserInvite();
		userInvite.setDisplayName(userEntity.getUserName());
		userInvite.setEmailAddress(userEntity.getEmailId());
		return userInvite;
	}

	public List<UserEntity> getAllUser() {
		List<UserEntity> list= userRepository.findAll();
		if(list.isEmpty())
			return Collections.emptyList();
		return list;
	}

	public Response revokeJiraInvitation(String jiraId) {
		RestTemplate restTemplate=new RestTemplate();
		String accountId = checkHaveAccess(jiraId);
		String stringToBeEncode =String.format("%s%s%s", adminUserName,":", apiKey) ;
		String requestURI = String.format("%s%s%s%s", jiraHostURI,createJiraUserURI,"?accountId=",accountId);
		String encodeString = new String(Base64.getEncoder().encode(stringToBeEncode.getBytes()));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", String.format("%s %s", "Basic", encodeString));
		HttpEntity httpEntity = new HttpEntity(headers);
		ResponseEntity<String> response=restTemplate.exchange(requestURI, HttpMethod.DELETE, httpEntity, String.class);

		if(response != null && response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			userEntity.setJiraId(null);
			userRepository.save(userEntity);
			Response returnResponse=new Response();
			returnResponse.setMessage(String.format("%s %s", revokeSuccess, userEntity.getUserName()));
			returnResponse.setStatusCode(HttpStatus.OK.value());
			return returnResponse;
		}else {
			throw JiraInviteException.somethingwentwrong();
		}
	}

	private String checkHaveAccess(String jiraId) {

		if(!StringUtils.hasText(jiraId)) {
			throw JiraInviteException.missingField("jiraId");
		}
		 userEntity=userRepository.findByJiraId(jiraId);
		if (userEntity==null){
			throw  JiraInviteException.somethingwentwrong();
		}
		return jiraId;
	}
}
