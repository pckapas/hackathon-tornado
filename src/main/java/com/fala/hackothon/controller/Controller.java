package com.fala.hackothon.controller;

import com.fala.hackothon.entity.UserEntity;
import com.fala.hackothon.entity.UserResponse;
import com.fala.hackothon.model.UserRequest;
import com.fala.hackothon.service.AzureAdService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fala.hackothon.model.Response;
import com.fala.hackothon.service.JIRAService;

@RestController
@RequestMapping(value = "/hackathon", produces = { "application/json"})
public class Controller {

	@Autowired
	private AzureAdService azureAdService;

	@Autowired
	private JIRAService service;

	@GetMapping("/send-invite")
	public ResponseEntity<Response> sendInvitationFromJira(@RequestParam String userMail) {
		return ResponseEntity.ok(service.sendInvitationFromJira(userMail));
	}

	@GetMapping("/list-users")
	public ResponseEntity<List<UserEntity>> users() {
		return ResponseEntity.ok(service.getAllUser());
	}

	@DeleteMapping("/revoke-jira-access")
	public ResponseEntity<Response> revokeJiraInvitation(@RequestParam String jiraId) {
		return ResponseEntity.ok(service.revokeJiraInvitation(jiraId));
	}

	@PostMapping("/user")
	public ResponseEntity<?> createUser(@RequestBody UserRequest request) throws Exception {
		azureAdService.createUser(request);
		if(request.isJira())
			service.sendInvitationFromJira(request.getEmail());
		return ResponseEntity.ok().build();
	}

	@GetMapping("/user")
	public ResponseEntity<UserResponse> getUser(@RequestParam String id) throws Exception {
		return ResponseEntity.ok(azureAdService.getUser(id));
	}

	@DeleteMapping("/user")
	public ResponseEntity<?> deleteUser(@RequestParam String id) throws Exception {
		azureAdService.deleteUser(id);
		return ResponseEntity.ok().build();
	}
}