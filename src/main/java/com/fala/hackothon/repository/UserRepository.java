package com.fala.hackothon.repository;

import java.util.UUID;

import javax.transaction.Transactional;

import com.fala.hackothon.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {
    public UserEntity findByEmailId(String emailId);

    public UserEntity findByJiraId(String jiraId);

    @Transactional
    public void deleteByEmailId(String emailId);
}
