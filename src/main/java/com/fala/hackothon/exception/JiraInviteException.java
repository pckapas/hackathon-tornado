package com.fala.hackothon.exception;

public class JiraInviteException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private JiraInviteException(String message) {
		super(message);
		
	}
	
	public static JiraInviteException missingField(String field) {
		return new JiraInviteException(String.format("%s%s",field,"is missing"));
	}

	public static JiraInviteException somethingwentwrong() {
		return new JiraInviteException("something went wrong");
	}


    public static JiraInviteException userNotFound() {
		return new JiraInviteException("user not found to jira access");
    }
}
