package com.fala.hackothon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnboardingToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnboardingToolApplication.class, args);
	}

}
