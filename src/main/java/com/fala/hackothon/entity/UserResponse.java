package com.fala.hackothon.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserResponse{

    @JsonProperty("@odata.context")
    public String odataContext;
    public String id;
    public List<Object> businessPhones;
    public String displayName;
    public String givenName;
    public String jobTitle;
    public String mail;
    public String mobilePhone;
    public String officeLocation;
    public String preferredLanguage;
    public String surname;
    public String userPrincipalName;

}