package com.fala.hackothon.entity;

import lombok.Data;

@Data
public class User{
    public boolean accountEnabled;
    public String displayName;
    public String mailNickname;
    public String userPrincipalName;
    public PasswordProfile passwordProfile;
}