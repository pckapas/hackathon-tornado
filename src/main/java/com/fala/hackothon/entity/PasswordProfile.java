package com.fala.hackothon.entity;

import lombok.Data;

@Data
public class PasswordProfile{
    public boolean forceChangePasswordNextSignIn;
    public String password;
}